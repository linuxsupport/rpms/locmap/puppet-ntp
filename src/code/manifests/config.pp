class ntp::config {

  file { '/etc/ntp.conf':
    ensure  => 'file',
    owner   => $ntp::params::owner,
    group   => $ntp::params::group,
    mode    => $ntp::params::mode,
    content => template('ntp/ntp.conf.erb'),
    require => Class['ntp::install'],
    notify  => Class['ntp::service'],
  }

  file { '/etc/ntp/step-tickers':
    ensure  => 'file',
    owner   => $ntp::params::owner,
    group   => $ntp::params::group,
    mode    => $ntp::params::mode,
    content => template('ntp/step-tickers.erb'),
    require => Class['ntp::install'],
    notify  => Class['ntp::service'],
  }

}
