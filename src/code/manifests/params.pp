class ntp::params {
  $domain       = lookup({"name" => "cern_network_domain", "default_value" => 'default'})
  $service_name = 'ntpd'
  $owner        = 'root'
  $group        = 'root'
  $mode         = '0644'

  if $domain == 'tn' {
    $ntpservers = ['ip-time-3.cern.ch','ip-time-4.cern.ch','ip-time-5.cern.ch']
  }
  else {
    $ntpservers = ['ip-time-0.cern.ch','ip-time-1.cern.ch','ip-time-2.cern.ch']
  }
}
