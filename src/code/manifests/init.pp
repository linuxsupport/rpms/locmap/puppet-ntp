class ntp {

  class {'ntp::params':}
  class {'ntp::install':}
  class {'ntp::config':}
  class {'ntp::service':}

}
