## puppet-ntp  
This module configures, based on the system's domain, the network time protocol.

*locmap will find the system's domain and pass the value to '/etc/puppet/hieradata/common.yaml'  
No user interaction is recommended.*