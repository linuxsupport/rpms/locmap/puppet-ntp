Name:           puppet-ntp
Version:        2.1
Release:        2%{?dist}
Summary:        Masterless puppet module for ntp

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for ntp.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/ntp/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/ntp/
touch %{buildroot}/%{_datadir}/puppet/modules/ntp/linuxsupport

%files -n puppet-ntp
%{_datadir}/puppet/modules/ntp
%doc code/README.md

%changelog
* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- fix requires on puppet-agent

* Thu Feb 11 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- move away from hieralookups

* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
